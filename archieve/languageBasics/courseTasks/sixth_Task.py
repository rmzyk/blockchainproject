import json
import pickle

#1 
# running = True
# while running:
#     print('Choose an option')
#     print('1. Add input')
#     print('2. Output data')
#     print('q: Quit')
#     user_input = input('Your Choice: ')
#     if user_input == '1':
#         data_to_store = input('Your text: ')
#         with open('sixth_task_resource.txt', mode='a') as file:
#             file.write(data_to_store)
#             file.write('\n')
#     elif user_input == '2':
#         with open('sixth_task_resource.txt', mode='r') as file:
#             file_content = file.readlines()
#             for line in file_content:
#                 print(line)
#     elif user_input == 'q':
#         running = False

# inputs = []
# running = True
# while running:
#     print('Choose an option')
#     print('1. Add input')
#     print('2. Output data')
#     print('q: Quit')
#     user_input = input('Your Choice: ')
#     if user_input == '1':
#         data_to_store = input('Your text: ')
#         inputs.append(data_to_store)
#         with open('sixth_task_resource.txt', mode='w') as file:
#             file.write(json.dumps(inputs))
#     elif user_input == '2':
#         with open('sixth_task_resource.txt', mode='r') as file:
#             file_content = json.loads(file.read())
#             for line in file_content:
#                 print(line)
#     elif user_input == 'q':
#         running = False


inputs = []
running = True
while running:
    print('Choose an option')
    print('1. Add input')
    print('2. Output data')
    print('q: Quit')
    user_input = input('Your Choice: ')
    if user_input == '1':
        data_to_store = input('Your text: ')
        inputs.append(data_to_store)
        with open('sixth_task_resource.p', mode='wb') as file:
            file.write(pickle.dumps(inputs))
    elif user_input == '2':
        with open('sixth_task_resource.p', mode='rb') as file:
            file_content = pickle.loads(file.read())
            for line in file_content:
                print(line)
    elif user_input == 'q':
        running = False