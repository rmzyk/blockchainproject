# 1.
person = [
    {
        'name': 'Rafal',
        'age': '22',
        'hobbies': ['programming', 'reading', 'family']
    },
    {
        'name': 'Josh',
        'age': '31',
        'hobbies': ['eating', 'footbal']
    },
    {
        'name': 'George',
        'age': '19',
        'hobbies': ['party', 'politics']
    },
    {
        'name': 'Marry',
        'age': '28',
        'hobbies': ['cooking', 'movies']
    }
    ]
# 2.
listOfNames = [el['name'] for el in person]
# 3.
areOlderThanTwenty = all([int(element['age']) > 20 for element in person])
# 4.
copyOfList = person[:]
# 5.
rafal, josh, george, marry = person

copyOfList.append('testData')

#1.
print(listOfNames)
#2.
print(areOlderThanTwenty)
#3.
print('old:', person)
#4.
print('new:', copyOfList)
#5.
print(rafal, josh, george, marry)
