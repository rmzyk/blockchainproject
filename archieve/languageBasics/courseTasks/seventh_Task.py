class Food:
    def __init__(self, name, kind):
        self.name = name
        self.kind = kind

    def __repr__(self):
        self.__dict__

    def describe(self):
        print(f'My name is {self.name} and I am {self.kind}')


class Meat(Food):
    def cook(self):
        print('I am cooking!')

class Fruit(Food):
    def cleaning(self):
        print('I am cleaning')