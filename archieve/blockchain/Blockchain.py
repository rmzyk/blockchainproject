from functools import reduce
from collections import OrderedDict
from api.domain.block import Block
from api.domain.transaction import Transaction
from api.helpers.hash_helper import hash_block
from api.helpers.validation_helper import Validator
import json
import pickle
MINING_REWARD = 10

class Blockchain:
    def __init__(self):
        GENESIS_BLOCK = Block(0, '', [], 100, 0)
        self.chain = [GENESIS_BLOCK]
        self.open_transactions = []
        self.load_data()

    def load_data(self):
        try:
            with open('blockchainResources.txt', mode='r') as file:
                # file_content = pickle.loads(file.read())
                file_content = file.readlines()
                # blockchain = file_content['chain']
                # open_transactions = file_content['open_transactions']
                blockchain = json.loads(file_content[0][:-1])
                updated_blockchain = []
                for block in blockchain:
                    converted_transaction = [Transaction(
                        transaction['sender'], transaction['recipient'], transaction['amount'], transaction['signature']) for transaction in block['transactions']]
                    updated_block = Block(block['index'], block['previous_hash'],
                                        converted_transaction, block['proof'], block['timestamp'])
                    updated_blockchain.append(updated_block)
                self.chain = updated_blockchain
                open_transactions = json.loads(file_content[1])
                updated_transactions = []
                for transaction in open_transactions:
                    updated_transaction = Transaction(
                        transaction['sender'], transaction['recipient'], transaction['amount'], transaction['signature'])
                    updated_transactions.append(updated_transaction)
                self.open_transactions = updated_transactions
        except (IOError, IndexError):
            print('Something')

    def save_data(self):
        try:
            with open('blockchainResources.txt', mode='w') as file:
                saveable_chain = [block.__dict__ for block in [Block(block_el.index, block_el.previous_hash, [
                    transaction.__dict__ for transaction in block_el.transactions], block_el.proof, block_el.timestamp) for block_el in self.chain]]
                file.write(json.dumps(saveable_chain))
                file.write('\n')
                saveable_transactions = [
                    transaction.__dict__ for transaction in self.open_transactions]
                file.write(json.dumps(saveable_transactions))
        except IOError:
            print('Saving failed')

    def proof_of_work(self):
        last_block = self.chain[-1]
        last_hash = hash_block(last_block)
        proof = 0
        validator = Validator()
        while not validator.is_proof_valid(self.open_transactions, last_hash, proof):
            proof += 1
        return proof


    def get_balance(self, participant):
        tx_sender = [[transaction.amount for transaction in block.transactions
                    if transaction.sender == participant] for block in self.chain]
        open_tx_sender = [transaction.amount
                        for transaction in self.open_transactions if transaction.sender == participant]
        tx_sender.append(open_tx_sender)
        amount_sent = reduce(
            lambda tx_sum, tx_amt: tx_sum + sum(tx_amt) if len(tx_amt) > 0 else tx_sum + 0, tx_sender, 0)

        tx_recipient = [[transaction.amount for transaction in block.transactions
                        if transaction.recipient == participant] for block in self.chain]
        amount_received = 0
        amount_received = reduce(
            lambda tx_sum, tx_amt: tx_sum + sum(tx_amt) if len(tx_amt) > 0 else tx_sum + 0, tx_recipient, 0)

        return amount_received - amount_sent


    def get_last_blockchain_value(self):
        if len(self.chain) < 1:
            return None
        return self.chain[-1]

    def add_transaction(self, recipient, sender, signature, amount=1.0):
        transaction = Transaction(sender, recipient, signature, amount)
        if Validator.verify_transaction(transaction, self.get_balance):
            self.open_transactions.append(transaction)
            self.save_data()
            return True
        return False


    def mine_block(self, node):
        last_block = self.chain[-1]
        hashed_block = hash_block(last_block)
        proof = self.proof_of_work()
        # reward_transaction = {
        #     'sender': 'MINING',
        #     'recipient': owner,
        #     'amount': MINING_REWARD
        # }
        reward_transaction = Transaction('MINING', node, MINING_REWARD)
        copied_transactions = self.open_transactions[:]
        copied_transactions.append(reward_transaction)

        print(hashed_block)

        block = Block(len(self.chain), hashed_block, copied_transactions, proof)

        self.chain.append(block)
        return True