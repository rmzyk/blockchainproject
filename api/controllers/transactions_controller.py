from flask import Blueprint, request
from api.domain.wallet import Wallet
from api.domain.blockchain import Blockchain
from api.services.transaction_service import TransactionService

def init_transactions_controller(blockchain: Blockchain, wallet: Wallet,
 service: TransactionService) -> Blueprint:
    transactions_controller = Blueprint('transaction', __name__)

    @transactions_controller.route('/transaction/create', methods=['POST'])
    def add_transaction():
        return service.add_transaction(blockchain, wallet, request.get_json())

    @transactions_controller.route('/transactions', methods=['GET'])
    def get_transactions():
        return service.get_open_transactions(blockchain, wallet)

    @transactions_controller.route('/broadcast-transaction', methods=['POST'])
    def broadcast_transaction():
        return service.broadcast_transaction(blockchain, wallet, request.get_json())

    return transactions_controller