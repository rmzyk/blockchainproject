from flask import Blueprint, request
from api.domain.wallet import Wallet
from api.domain.blockchain import Blockchain
from api.services.node_service import NodeService

def init_node_controller(blockchain: Blockchain, wallet: Wallet,
 service: NodeService) -> Blueprint:
    node_controller = Blueprint('node', __name__)

    @node_controller.route('/nodes/create', methods=['POST'])
    def add_node():
        return service.add_node(blockchain, wallet, request.get_json())

    @node_controller.route('/nodes/<node_url>', methods=['DELETE'])
    def remove_node(node_url):
        return service.remove_node(blockchain, node_url)

    @node_controller.route('/nodes', methods=['GET'])
    def get_all():
        return service.get_all_nodes(blockchain)
    
    return node_controller