from api.domain.wallet import Wallet
from api.domain.blockchain import Blockchain
from api.helpers.response_helper import HttpResponse

class TransactionService:
    def __init__(self):
        pass

    def add_transaction(self, blockchain: Blockchain, wallet: Wallet, request: str) -> str:  
        required_fields = ['recipient', 'amount']
        if not request or not all(field in request for field in required_fields or wallet.public_key is None):
            return HttpResponse.get_response('Failure', 400, 'Something went wrong', 'Invalid data provided.')
        signature = wallet.sign_transaction(wallet.public_key, request['recipient'], float(request['amount']))
        response = blockchain.add_transaction(request['recipient'], wallet.public_key, signature, float(request['amount']))

        if response:
             return HttpResponse.get_response('Success', 201, 
             {
                 'transaction':
                     {
                         'sender': wallet.public_key, 
                         'recipient': request['recipient'], 
                         'amount': request['amount'],
                         'signature': signature
                         },
                  'balance': blockchain.get_balance()})
        else:
             return HttpResponse.get_response('Failure', 400, 'Something went wrong', 'Adding transaction failed.')

    def get_open_transactions(self, blockchain: Blockchain, wallet: Wallet) -> str:
        transactions = blockchain.get_open_transactions()
        dict_transactions = [transaction.__dict__ for transaction in transactions]
        return HttpResponse.get_response('Success', 200, {'transactions': dict_transactions})

    def broadcast_transaction(self, blockchain: Blockchain, wallet: Wallet, request: str) -> str:
        required = ['sender', 'recipient', 'amount', 'signature']
        if not request or not all(key in request for key in required):
             return HttpResponse.get_response('Failure', 400, 'Something went wrong', 'Invalid data provided.')
        response = blockchain.add_transaction(request['recipient'], request['sender'], request['signature'], request['amount'], is_receiving=True)
        if response:
            return HttpResponse.get_response('Success', 204, 
            { 'message': 'Transaction broadcasted successfully.',
                'transaction':
                {
                    'sender': request['sender'],
                    'recipient': request['recipient'],
                    'signature': request['signature'],
                    'amount': request['amount']
                }})
        return HttpResponse.get_response('Failure', 400, 'Broadcasting transaction failed.')
 

