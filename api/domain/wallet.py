from Crypto.PublicKey import RSA
from Crypto.Signature import PKCS1_v1_5
from Crypto.Hash import SHA256
import Crypto.Random
import binascii

class Wallet:
    def __init__(self, node_id: str) -> None:
        self.private_key = None
        self.public_key = None
        self.node_id = node_id

    def create_key(self):
         self.private_key, self.public_key = self.generate_key()

    def load_key(self) -> bool:
        try:
            with open(f'api/data/wallet-snapshot-{self.node_id}.txt', mode='r') as file:
                keys = file.readlines()
                self.public_key = keys[0][:-1]
                self.private_key = keys[1]
                return True
        except(IOError, IndexError):
                return False


    def save_keys(self) -> bool:
        if self.public_key != None and self.private_key != None:
            try:
                with open(f'api/data/wallet-snapshot-{self.node_id}.txt', mode='w') as file:
                    file.write(self.public_key)
                    file.write('\n')
                    file.write(self.private_key)
                    return True
            except (IOError, IndexError):
                return False
 
    def generate_key(self):
       key = RSA.generate(1024, Crypto.Random.new().read)
       return (binascii.hexlify(key.export_key(format='DER')).decode('ascii'),
        binascii.hexlify(key.publickey().export_key(format='DER')).decode('ascii'))

    def sign_transaction(self, sender, recipient, amount) -> None:
        signer = PKCS1_v1_5.new(RSA.import_key(binascii.unhexlify(self.private_key)))
        payload = SHA256.new((f'{str(sender)}{str(recipient)}{str(amount)}').encode('utf8'))
        signature = signer.sign(payload)
        return binascii.hexlify(signature).decode('ascii')

    @staticmethod
    def verify_transaction(transaction):
        key = RSA.importKey(binascii.unhexlify(transaction.sender))
        validator = PKCS1_v1_5.new(key)
        payload = SHA256.new((f'{str(transaction.sender)}{str(transaction.recipient)}{str(transaction.amount)}').encode('utf8'))
        return validator.verify(payload, binascii.unhexlify(transaction.signature))


    