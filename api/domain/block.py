from time import time
from api.domain.printable import Printable


class Block(Printable):
    def __init__(self, index: int, previous_hash: str, transactions: list,
                 proof: int, timestamp: int = None) -> None:
        self.index = index
        self.previous_hash = previous_hash
        self.transactions = transactions
        self.proof = proof
        self.timestamp = time() if timestamp is None else timestamp

    def __repr__(self):
        return str(self.__dict__)
